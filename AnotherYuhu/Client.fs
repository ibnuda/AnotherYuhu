namespace AnotherYuhu

open System
open WebSharper
open WebSharper.JavaScript
open WebSharper.JQuery
open WebSharper.UI.Next
open WebSharper.UI.Next.Client

[<JavaScript>]
module Domain =
  type Claim =
    { Id: string
      Name: String }

  type Claims = Claim list

  type User =
    { Id: string
      FullName: string
      EmailAddress: string option
      PhoneNumber: string option
      Enabled: bool
      Claims: Claims }

[<JavaScript>]
module Async =
  let map f x =
    async {
      let! x = x
      return f x }
  let retn x =
    async { return x }

[<JavaScript>]
module AsyncApi =
  type ApiResult<'a> =
    | Success of 'a
    | Failure of ApiResponseException list
  and ApiResponseException =
    | Unauthorized of string
    | NotFound of string
    | UnsupportedMediaType of string
    | BadRequest of string
    | JsonDeserializeError of string
    override this.ToString () =
      match this with
      | ApiResponseException.Unauthorized err
      | ApiResponseException.NotFound err
      | ApiResponseException.UnsupportedMediaType err
      | ApiResponseException.BadRequest err
      | ApiResponseException.JsonDeserializeError err
        -> err
  let map f x =
    async
      { let! x = x
        match x with
        | Success x -> return Success (f x)
        | Failure err -> return Failure err }

  let retn x =
    async { return ApiResult.Success x }

  let bind f x =
    async
      { let! x = x
        match x with
        | Success x -> return! f x
        | Failure err -> return (Failure err) }

  let start x =
    x
    |> Async.map (fun x -> ())
    |> Async.Start

  type ApiCallBuilder () =
    member this.Bind (x, f) =
      async
        { let! x = x
          match x with
          | Success x -> return! f x
          | Failure err -> return (Failure err) }
    member this.Return x =
      async { return ApiResult.Success x }
    member this.ReturnFrom x = x

[<JavaScript>]
module ApiClient =
  open WebSharper.JavaScript
  open WebSharper.JQuery
  open AsyncApi
  open Domain
  open WebSharper.UI.Next

  type AuthToken =
    { Token: string
      Expiry: DateTime }

    member this.Expired () = DateTime.UtcNow - this.Expiry < TimeSpan.FromMinutes (10.0)
    
    static member Make token =
      { Token = token
        Expiry = DateTime.UtcNow }

    static member Default =
      { Token = ""
        Expiry = DateTime.UtcNow }

  type ValidToken =
  | ValidToken of string

  type Credentials =
    { Username: string
      Password: string }
    static member Default =
      { Username = "admin"
        Password = "admin" }

  type RequestSettings =
    { RequestType: JQuery.RequestType
      Url: string
      ContentType: string option
      Headers: (string * string) list option
      Data: string option }

    member this.ToAjaxSettings ok ko =
      let settings =
        JQuery.AjaxSettings
          ( Url = "http://localhost/api" + this.Url
          , Type = this.RequestType
          , DataType = JQuery.DataType.Text
          , Success = (fun (res, _, _) -> ok (res :?> string))
          , Error = (fun (jqXHR, _, _) -> ko (System.Exception (string jqXHR.Status))))
      this.Headers |> Option.iter (fun h -> settings.Headers <- Object<string> (h |> Array.ofList))
      this.ContentType |> Option.iter (fun c -> settings.ContentType <- c)
      this.Data |> Option.iter (fun d -> settings.Data <- d)
      settings

  type Api =
    { Login: Credentials -> Async<ApiResult<unit>>
      Logout: unit -> unit
      GetUsers: unit -> Async<ApiResult<User list>>
      GetClaims: unit -> Async<ApiResult<Claims>> }

  [<Literal>]
  let TokenStorageKey = "authtoken"

  let private ajaxCall (requestSettings: RequestSettings) =
    Async.FromContinuations <| fun (ok, ko, _) ->
      requestSettings.ToAjaxSettings ok ko
      |> JQuery.Ajax
      |> ignore



[<JavaScript>]
module Client =    
    type IndexTemplate = Templating.Template<"index.html">

    let People =
        ListModel.FromSeq [
            "John"
            "Paul"
        ]

    let Main =
        JQuery.Of("#main").Empty().Ignore

        let newName = Var.Create ""

        IndexTemplate.Main.Doc(
            ListContainer = [
                People.View.DocSeqCached(fun name ->
                    IndexTemplate.ListItem.Doc(Name = View.Const name)
                )
            ],
            Name = newName,
            Add = (fun el ev ->
                People.Add(newName.Value)
                newName.Value <- ""
            )
        )
        |> Doc.RunById "main"
